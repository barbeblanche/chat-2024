import { BadRequest } from "@feathersjs/errors";

import bcrypt from 'bcryptjs'
import { uploadToLocal } from "../../helpers/uploadToLocal.js";


export const userPatchBefore = async (context) => {
  if(context.params?.user) {
    context.id = context.params.user._id;
  
    if(!context.data?.action || !['UPDATE_INFOS', 'CHANGE_PASSWORD'].includes(context.data.action)) {
      throw new BadRequest("You have to specified a valid action (UPDATE_INFOS, CHANGE_PASSWORD)")
    }
    
    if (context.data.action === 'UPDATE_INFOS') {
      const {email, firstname, lastname, status} = context.data;
      context.data = {
        email,
        firstname,
        lastname,
        status
      }
    }
  
    else if (context.data.action === 'CHANGE_PASSWORD') {
      const {currentPassword, newPassword} = context.data;
  
      if (!currentPassword || !newPassword) {
        throw new BadRequest("You must provide your current and new password")
      }
  
      const hash = context.params?.user?.password
  
      const result = await bcrypt.compare(currentPassword, hash)
  
      if (!result) {
        throw new BadRequest("Your current password is incorrect")
      }
  
      context.data = {
        password : newPassword
      }
    }
  
    else {
      context.data = {};    
    }

  }
}

export const userUploadFile = async (context) => {

  if (context.params.file) {
      var i = 0
      const { originalname, buffer, mimetype, size } = context.params.file
      let finalPath = await uploadToLocal(originalname, buffer, i.toString(), 'public/profils')
      context.data.photoUrl = finalPath
    }
}

export const userPatchAfter = async (context) => {

}
