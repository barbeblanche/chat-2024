import { BadRequest } from "@feathersjs/errors"

export const surveyPatchBefore = async (context) => {


  if (context.params?.user) {

    var survey = await context.app.service('surveys').get(context.id);

    var discussion = await context.app.service('discussions').get(survey.discussionId);

    const finduser = discussion.members.find( participant => participant.userId.toString() == context.params.user._id.toString());

    if (!finduser) {
      throw new BadRequest("You can not access to this survey");
    }

    const possibleActions = ['ASK_SURVEY', 'UPDATE_SURVEY']

    if (!context.data.action || !possibleActions.includes(context.data.action)) {
      throw new BadRequest("Invalid action");
    }


    for (var i = 0; i < survey.options.length; i++){
      survey.options[i].voters = survey.options[i].voters.map(user => user._id)  
    }

    if (context.data.action === 'ASK_SURVEY') {

      if (!context.data.optionId || context.data.isSelected === undefined) {
        throw new BadRequest("You must provide option id and your response")
      }

      const findOptionIndex = survey.options.findIndex(option => option.id  === context.data.optionId);
      if (findOptionIndex < 0) {
        throw new BadRequest("This option not existed")
      }
      if (context.data.isSelected === true) {
        const userAlreadySelected = survey.options[findOptionIndex].voters.find(user => context.params.user._id.toString() === user.toString())
        if (!userAlreadySelected) {
          survey.options[findOptionIndex].voters.push(context.params.user._id)
        }
      } 
      else {
        survey.options[findOptionIndex].voters = survey.options[findOptionIndex].voters.filter(user => context.params.user._id.toString() !== user.toString())
      }

      context.data = {
        options: survey.options
      }
      
    }

    delete context.data?.action;

  }

}

export const discussionPatchAfter = async (context) => {

}
