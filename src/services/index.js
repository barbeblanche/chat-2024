import { survey } from './surveys/surveys.js'

import { download } from './download/download.js'

import { message } from './messages/messages.js'

import { discussion } from './discussions/discussions.js'

import { contact } from './contacts/contacts.js'

import { user } from './users/users.js'

export const services = (app) => {
  app.configure(survey)

  app.configure(download)

  app.configure(message)

  app.configure(discussion)

  app.configure(contact)

  app.configure(user)

  // All services will be registered here
}
