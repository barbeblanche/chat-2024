// For more information about this file see https://dove.feathersjs.com/guides/cli/service.html
import { authenticate } from '@feathersjs/authentication'

import { hooks as schemaHooks } from '@feathersjs/schema'
import {
  contactDataValidator,
  contactPatchValidator,
  contactQueryValidator,
  contactResolver,
  contactExternalResolver,
  contactDataResolver,
  contactPatchResolver,
  contactQueryResolver
} from './contacts.schema.js'
import { ContactService, getOptions } from './contacts.class.js'
import { contactCreateBefore } from '../../hooks/contacts/contact-create-hook.js'
import { contactFindBefore } from '../../hooks/contacts/contact-find-hook.js'
import { contactPatchAfter, contactPatchBefore } from '../../hooks/contacts/contact-patch-hook.js'
import { contactRemoveBefore } from '../../hooks/contacts/contact-remove-hook.js'

export const contactPath = 'contacts'
export const contactMethods = ['find', 'get', 'create', 'patch', 'remove']

export * from './contacts.class.js'
export * from './contacts.schema.js'

// A configure function that registers the service and its hooks via `app.configure`
export const contact = (app) => {
  // Register our service on the Feathers application
  app.use(contactPath, new ContactService(getOptions(app)), {
    // A list of all methods this service exposes externally
    methods: contactMethods,
    // You can add additional custom events to be sent to clients here
    events: []
  })
  // Initialize hooks
  app.service(contactPath).hooks({
    around: {
      all: [
        authenticate('jwt'),
        schemaHooks.resolveExternal(contactExternalResolver),
        schemaHooks.resolveResult(contactResolver)
      ]
    },
    before: {
      all: [schemaHooks.validateQuery(contactQueryValidator), schemaHooks.resolveQuery(contactQueryResolver)],
      find: [contactFindBefore],
      get: [],
      create: [contactCreateBefore, schemaHooks.validateData(contactDataValidator), schemaHooks.resolveData(contactDataResolver)],
      patch: [contactPatchBefore, schemaHooks.validateData(contactPatchValidator), schemaHooks.resolveData(contactPatchResolver)],
      remove: [contactRemoveBefore]
    },
    after: {
      all: [],
      patch: [contactPatchAfter]
    },
    error: {
      all: []
    }
  })
}
